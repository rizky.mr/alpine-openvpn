FROM alpine:latest

RUN apk add --update openvpn && rm -rf /tmp/* /var/cache/apk/*

ADD run-openvpn.sh /usr/local/bin/
CMD ["/usr/local/bin/run-openvpn.sh"]